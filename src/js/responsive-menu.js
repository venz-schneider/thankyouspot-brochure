
var xDown = null,
	yDown = null;

const 	body = document.querySelector("body"),
		navMenu = document.querySelector(".nav-menu"),
		hamburger = document.querySelector(".hamburger"),
		downButton = document.querySelector(".down-button"),
		secondaryNav = document.querySelector(".secondary-nav");

const toggleMenuIcon = () => {

	var screenWidth = document.documentElement.clientWidth;

	if ( ! navMenu.classList.contains("open") ){

		hamburger.classList.remove("close");
		if ( screenWidth > 480 && screenWidth <= 768 ){
			hamburger.removeAttribute("style");
			if (secondaryNav && document.querySelector(".secondary-nav+section")) {
				secondaryNav.removeAttribute("style");
				document.querySelector(".secondary-nav+section").removeAttribute("style");
			}
		}

	}
	else{

		hamburger.classList.add("close");
		if ( screenWidth > 480 && screenWidth <= 768 ){
			if ( secondaryNav ) {
				secondaryNav.style.position = "fixed";
				document.querySelector(".secondary-nav+section").style.paddingTop = "121px";
			}
			hamburger.style.transform = "translate(50vw," + document.documentElement.scrollTop + "px)";
			
		}

	}
}                                                       

const handleTouchStart = (evt) => {                                     
    xDown = evt.touches[0].clientX;                                      
    yDown = evt.touches[0].clientY;                                      
}; 

const handleTouchMove = (evt) => {

    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    var screenWidth = document.documentElement.clientWidth;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */
            if ( xDiff > 10 &&
            	! hamburger.classList.contains('close') &&
            	screenWidth > 480 && screenWidth <= 768 ) {

				showMenu();
            }

        } else {
            /* right swipe */
            if ( xDiff < -10 &&
            	hamburger.classList.contains('close') &&
            	screenWidth > 480 && screenWidth <= 768 ) {

            	hideMenu();
            }

        }                       
    }

    /* reset values */
    xDown = null;
    yDown = null;     
};

const hideMenu = () => {

	var screenWidth = document.documentElement.clientWidth;

	body.removeAttribute("style");
	navMenu.classList.remove("open");
	navMenu.style.display = screenWidth > 480 && screenWidth <= 768 ? "flex" : "none";

	if ( screenWidth > 480 && screenWidth <= 768 )
		body.classList.remove("half");

	if( downButton )
		downButton.style.display = (document.documentElement.scrollHeight - window.scrollY) <= 
			window.innerHeight ? "none" : "flex";
	if ( screenWidth <= 480 )
		document.querySelector(".nav-title-a .nav-title").removeAttribute("style");

	toggleMenuIcon();
};

const showMenu = () => {

	var screenWidth = document.documentElement.clientWidth;

	navMenu.style.display = "flex";
	navMenu.classList.add("open");
	body.style.overflow = "hidden";

	if ( downButton ){
		downButton.style.display = "none";
	}

	if ( screenWidth > 480 && screenWidth <= 768 ){
		navMenu.style.transform = "translate(50vw," + document.documentElement.scrollTop + "px)";
		body.style.transform = "translateX(-50vw)";
	}

	if ( screenWidth <= 480 ) {
		document.querySelector(".nav-title-a .nav-title").style.position = "relative";
		document.querySelector(".nav-title-a .nav-title").style.zIndex = "3";
	}

	toggleMenuIcon();
};

hamburger.addEventListener('click', (e) => {

	if( hamburger.classList.contains('close') ){
		hideMenu();
	}else{
		showMenu();
	}
});

body.addEventListener('click', (e) => {
	if( hamburger.classList.contains('close') && 
		(e.target != hamburger && e.target != navMenu && 
			e.target != document.querySelector(".nav-menu-list") &&
			e.target != document.querySelector(".nav-menu-a"))){
		hideMenu();
	}
});

window.addEventListener('scroll', (e) => {

	var screenWidth = document.documentElement.clientWidth;

	if ( screenWidth > 480 &&  screenWidth <= 768 ){
		navMenu.style.transform = "translate(50vw," + document.documentElement.scrollTop + "px)";
	}
	if( downButton ){
		downButton.style.display = 
			(document.documentElement.scrollHeight - window.scrollY) <= 
				window.innerHeight ? "none" : "flex";
	}
});

window.addEventListener('resize', () => {

	var screenWidth = document.documentElement.clientWidth;

	if ( navMenu.classList.contains("open") ) {
		if ( screenWidth > 480 &&  screenWidth <= 768 ){
			navMenu.style.display = "flex";
			navMenu.style.transform = "translate(50vw," + document.documentElement.scrollTop + "px)";
			body.style.transform = "translateX(-50vw)";
			body.style.overflow = "hidden";
		}
		if ( screenWidth <= 480 ){
			body.style.transform = "";
			navMenu.style.transform = "";
			document.querySelector(".nav-title-a .nav-title").style.zIndex = "3";
			document.querySelector(".nav-title-a .nav-title").style.position = "relative";
			hamburger.removeAttribute("style");
			if ( downButton )
				downButton.style.display = "none";
		}
		if ( screenWidth > 768 ){
			navMenu.classList.remove("open");
			navMenu.removeAttribute("style");
			body.removeAttribute("style");
			secondaryNav.removeAttribute("style");
			toggleMenuIcon();
		}
	}else{

		if ( screenWidth > 480 &&  screenWidth <= 768 ){
			navMenu.style.display = "flex";
			navMenu.style.transform = "translate(50vw," + document.documentElement.scrollTop + "px)";
		}
		if ( screenWidth <= 480 ){
			body.removeAttribute("style");
			navMenu.removeAttribute("style");
			if ( downButton )
				downButton.style.display = "flex";
		}
		if ( screenWidth > 768 ){
			navMenu.removeAttribute("style");
			body.removeAttribute("style");
		}
	}

});

document.addEventListener('touchstart', handleTouchStart,false);        
document.addEventListener('touchmove', handleTouchMove,false);



