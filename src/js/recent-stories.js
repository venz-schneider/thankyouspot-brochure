

const loadRecentStories = () => {

  var limit = 12,
      offset = 0,
      xDown = null,
      yDown = null;

  var thankyou_container = document.querySelector(".story-thankyou-container"),
      close_icon = document.querySelector(".story-modal-close-icon"),
      try_again_button = document.querySelector(".try-again-button"),
      load_more_button = document.querySelector(".load-more-button"),
      story_popup = document.querySelector(".story-popup"),
      body = document.querySelector("body"),
      entry_text = story_popup.querySelector(".mid_entry_text"),
      ss_content = entry_text.querySelector(".ss-content");

  const toggleThankYou = () => {

    if (thankyou_container.classList.contains("open")) {

      thankyou_container.removeAttribute("style");
      thankyou_container.classList.remove("open");
      thankyou_container.scrollTop = "0";
      close_icon.classList.remove("chevron");
      document.querySelector(".story-image").classList.remove("zoomed");

    }else{

      thankyou_container.style.transform = "translate(0px, 0px)";
      thankyou_container.classList.add("open");
      close_icon.classList.add("chevron");
      document.querySelector(".story-image").classList.add("zoomed");
    }

  };

  const toggleModal = () => {

    if (story_popup.classList.contains("open")){

        story_popup.style.display = "none";
        story_popup.classList.remove("open");
        body.removeAttribute("style");
        ss_content.scrollTop = 0;

        if (thankyou_container.classList.contains("open")) 
          toggleThankYou();

    }else{

      story_popup.style.display = "flex";
      story_popup.classList.add("open");
      body.style.overflow = "hidden";

    }

  };

  const handleContentScroll = () => {

    var classes = ["scrolled-top","scrolled-bottom", "scrolled-middle"];

    if (ss_content.clientHeight < ss_content.scrollHeight) {
      if (ss_content.scrollTop == 0 ) {
        entry_text.classList.remove(...classes);
        entry_text.classList.add("scrolled-top");
      }
      if (ss_content.scrollTop == ( ss_content.scrollHeight - ss_content.clientHeight ) ) {
        entry_text.classList.remove(...classes);
        entry_text.classList.add("scrolled-bottom");
      }
      if (ss_content.scrollTop < (ss_content.scrollHeight - ss_content.clientHeight) && ss_content.scrollTop > 0 ) {
        entry_text.classList.remove(...classes);
        entry_text.classList.add("scrolled-middle");
      }

    }else{
      entry_text.classList.remove(...classes);
    }

  }

  const handleTouchStart = (evt) => {                                     
    xDown = evt.touches[0].clientX;                                      
    yDown = evt.touches[0].clientY;                                      
  }; 

  const handleTouchMove = (evt) => {

    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    var screenWidth = document.documentElement.clientWidth;

    if( yDiff > 15 && ! thankyou_container.classList.contains("open") )
      toggleThankYou();

    if( yDiff < -15 )
      if ( thankyou_container.classList.contains("open") && thankyou_container.scrollTop == 0 )
        toggleThankYou();

    /* reset values */
    xDown = null;
    yDown = null;     

  };

  const formatDate = (date) => {

    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var weekDays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    var day = date.getDay();
    var monthDay = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return weekDays[day] + ' ' + monthNames[monthIndex] + ' ' + monthDay + ', ' + year;
  }

  const getTimeDifferenceForDate = (previous) => {

    const milliSecondsPerMinute = 60 * 1000;
    const milliSecondsPerHour = milliSecondsPerMinute * 60;
    const milliSecondsPerDay = milliSecondsPerHour * 24;
    const milliSecondsPerWeek = milliSecondsPerDay * 7;
    const milliSecondsPerMonth = milliSecondsPerDay * 30;
    const milliSecondsPerYear = milliSecondsPerMonth * 12;
    const current = new Date().getTime();
    const elapsed = current - (previous*1000);

    if(elapsed < milliSecondsPerMinute / 3){
        return 'just now';
    }

    if(elapsed < milliSecondsPerMinute ){
        return 'less than a minute ago';
    }
    else if(elapsed < milliSecondsPerHour){
        return Math.round(elapsed/milliSecondsPerMinute) + ' min ago';
    }

    else if(elapsed < milliSecondsPerDay){
        return Math.round(elapsed/milliSecondsPerHour) + ' h ago';
    }

    else if(elapsed < milliSecondsPerWeek){
      return Math.round(elapsed/milliSecondsPerDay) + ' d ago';
    }

    else if(elapsed < milliSecondsPerMonth){
      return Math.round(elapsed/milliSecondsPerWeek) + ' wk ago';
    }
    
    else if(elapsed < milliSecondsPerYear){
      return Math.round(elapsed/milliSecondsPerMonth) + ' mo ago';
    }
    else{

      return formatDate(new Date(previous*1000));

    }

  }

  const loadStories = () => {

    // Connect...
    var graph = graphql("http://api.staging.thankyouspot.com/graphql");

    // Prepare...
    var allStories = graph(`
        query{
          recentStories(limit: ${limit}, offset: ${offset}){
              id
              content
              color
              timestamp
              fromUser{
                id
                name
                photo{
                  id
                  src
                }
              }
              toRecipient{
                id
                name
              }
              images{
                id
                src
                height
                width
              }
          }
        }
    `);

    document.querySelector(".stories-loader-icon").style.display = "block";
    document.querySelector(".load-more-button").style.display = "none";
    document.querySelector(".thank_yous_header").style.display = "flex";
    document.querySelector(".failed-to-load").style.display = "none";

    allStories().then((stories)=>{

        var thanksEntries = document.querySelector(".latest_thanks_entries");
        
        stories.recentStories.map( (story, i) => {
            
            var tempLink = document.querySelector("#cardTemplate").import;
            var cardNode = tempLink.querySelector("#story-template");
            var storyNode = document.importNode(cardNode.content, true);

            storyNode.querySelector(".left_top_entry_photo")
              .style.backgroundImage = "url('" + 
                ( story.fromUser.photo ? 
                  story.fromUser.photo.src : 
                  '../images/defaultPics/dp' + ( ( story.fromUser.id % 10 ) + 1 ) + '.png' ) + "')";

            storyNode.querySelector(".mid_entry_bottom_photo")
              .style.backgroundImage = "url('" + 
                ( story.fromUser.photo ? 
                  story.fromUser.photo.src : 
                  '../images/defaultPics/dp' + ( ( story.fromUser.id % 10 ) + 1 ) + '.png' ) + "')";

            if (story.images.length){

                storyNode.querySelector(".mid_entry_photo")
                  .style.backgroundImage = "url('" + story.images[0].src + "')";

            }else{

                storyNode.querySelector(".mid_entry_photo")
                  .classList.add("mid_entry_photo_empty");

                storyNode.querySelector(".mid_entry_photo_empty")
                  .classList.remove("mid_entry_photo");
                
                storyNode.querySelector(".mid_entry_text_text")
                  .classList.add("text_no_image");

            }

            storyNode.querySelector(".mid_entry_text_text").textContent = 
              ( story.images.length ? 
                story.content.substr( 0, 50 ) : 
                story.content.substr( 0, 230 ) ) + 
                  ( story.content.length > 54 && story.images.length ? 
                    " ..." : 
                    ( story.content.length > 234 && !story.images.length ? "..." : "" ) );

            storyNode.querySelector(".a_thank_name").textContent = story.toRecipient.name;
            storyNode.querySelector(".mid_entry_bottom_text .from").textContent = story.fromUser.name;
            storyNode.querySelector(".mid_entry_bottom_text .time_from").textContent = getTimeDifferenceForDate(story.timestamp);
            storyNode.querySelector(".mid_entry").className += " bg_" + story.color.toLowerCase();
            storyNode.querySelector(".entry").addEventListener('click', function(){

              var storyModal = story_popup;

              console.log("entry clicked");

              storyModal.querySelector(".left_top_entry_photo")
                .style.backgroundImage = "url('" + 
                  ( story.fromUser.photo ? 
                    story.fromUser.photo.src : 
                    '../images/defaultPics/dp' + ( (story.fromUser.id % 10) + 1 ) + '.png' ) + "')";

              storyModal.querySelector(".mid_entry_bottom_photo")
                .style.backgroundImage = "url('" + 
                  ( story.fromUser.photo ? 
                    story.fromUser.photo.src : 
                    '../images/defaultPics/dp' + ( (story.fromUser.id % 10) + 1 ) + '.png' ) + "')";

              if (story.images.length){

                  storyModal.querySelector(".story-image").src = story.images[0].src;
                  storyModal.querySelector(".story-image").style.display = "flex";
                  storyModal.classList.add("with-image");

              }else{

                  storyModal.querySelector(".story-image").style.display = "none";
                  storyModal.querySelector(".story-image").removeAttribute("style");
                  storyModal.classList.remove("with-image");
              }

              let bgColors = ["bg_green", "bg_red", "bg_blue", "bg_pink", "bg_gray", "bg_orange", "bg_purple", "bg_yellow"];

              storyModal.querySelector(".a_thank_name").textContent = story.toRecipient.name;
              storyModal.querySelector(".mid_entry_text_text").textContent = story.content;
              storyModal.querySelector(".mid_entry_bottom_text .from").textContent = story.fromUser.name;
              storyModal.querySelector(".mid_entry_bottom_text .time_from").textContent = getTimeDifferenceForDate(story.timestamp);
              storyModal.querySelector(".story-thankyou-container").classList.remove(...bgColors);
              storyModal.querySelector(".story-thankyou-container").classList.add("bg_" + story.color.toLowerCase());

              toggleModal();
              handleContentScroll();

            });
            
            setTimeout(function(){thanksEntries.appendChild(storyNode)} , i * 50 );

        });

        document.querySelector(".stories-loader-icon").style.display = "none";

        if ( stories.recentStories.length == limit ) {
          document.querySelector(".load-more-button").style.display = "block";
          offset += limit;
        } 

      }).catch((err) => {
        console.error(err);
        document.querySelector(".latest_thanks_entries").style.display = "none";
        document.querySelector(".stories-loader-icon").style.display = "none";
        document.querySelector(".thank_yous_header").style.display = "none";
        document.querySelector(".load-more-button").style.display = "none";
        document.querySelector(".failed-to-load").style.display = "flex";
    });

  };

  loadStories();

  try_again_button.addEventListener('click', loadStories);
  load_more_button.addEventListener('click', loadStories);

  story_popup.addEventListener('click', function(e){
    if (e.target == this)
      toggleModal();
  });


  thankyou_container.addEventListener('click', function(e){
    if ( ! thankyou_container.classList.contains("open") && e.target != close_icon)
      toggleThankYou();
  });

  close_icon.addEventListener('click', function(){

    if ( thankyou_container.classList.contains("open") ) {

      toggleThankYou();

    }else{

      story_popup.style.display = "none";
      story_popup.classList.remove("open");
      body.removeAttribute("style");

    }
  
  });

  story_popup.addEventListener('touchstart', handleTouchStart);
  story_popup.addEventListener('touchmove', handleTouchMove);
  ss_content.addEventListener('scroll', function(){
    handleContentScroll();  
  });

  document.addEventListener('keyup', function(e){

    if (e.keyCode == 27 && story_popup.classList.contains("open")){

      if (thankyou_container.classList.contains("open"))
        toggleThankYou();
      else
        toggleModal();
    }

  });

}

window.addEventListener('load', loadRecentStories);

