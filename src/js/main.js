
if(navigator.serviceWorker) {
  navigator.serviceWorker.register('/sw.js')
  .catch(function(err) {
    console.error('Unable to register service worker.', err);
  });
}

const animator = new Worker('build/worker.min.js');

function animate(startTime) {
  animator.postMessage(startTime)
  requestAnimationFrame(animate)
}

requestAnimationFrame(animate)

const mainGraphic = document.querySelector('.phone')

// bodymovin.loadAnimation({
//   container: mainGraphic, // the dom element that will contain the animation
//   renderer: 'svg',
//   loop: true,
//   autoplay: true,
//   path: './images/data.json' // the path to the animation json
// });